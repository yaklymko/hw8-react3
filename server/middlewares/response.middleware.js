const responseMiddleware = (req, res, next) => {
    if (req.err) {
        const statusCode = req.err.code || 404;
        const message = req.err.message || "error";

        res.status(statusCode).json({
            error: true,
            message: message,
        });
        next();
        return;
    }
    res.status(200).json(res.data);
    next();
}

exports.responseMiddleware = responseMiddleware;