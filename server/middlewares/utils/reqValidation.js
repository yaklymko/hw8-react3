exports.checkOddFields = function (reqBody, templ) {
    for (let prop in reqBody) {
        if (!(prop in templ)) {
            throw new Error(`Odd field [${prop}] shouldn't be in request body`);
        }
    }
}

exports.checkIfHasReqFields = function (reqBody, template) {
    for (let prop in template) {
        if (!(prop in reqBody)) {
            throw new Error(`Req Body doesn't have ${prop} field`);
        }
    }
}

exports.checkFieldRange = function (value, minValue, maxValue, fieldName = "") {
    value = parseInt(value)
    if (!value) {
        throw new Error(`Field [${fieldName }] must be a number`);
    }
    if (value < minValue || value > maxValue) {
        throw new Error(`Field [${fieldName }] must be beween [${minValue}] and [${maxValue}]`);
    }
}