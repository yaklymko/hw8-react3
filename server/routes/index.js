const userRoutes = require('./userRoutes');
const messageRoutes = require('./messageRoutes');

module.exports = (app) => {
    app.use('/api/Users', userRoutes);
    app.use('/api/messages', messageRoutes);
  };