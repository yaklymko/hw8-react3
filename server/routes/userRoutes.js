const {
    Router
} = require('express');
const UserService = require('../services/userService');
const {
    createUserValid,
    updateUserValid
} = require('../middlewares/user.validation.middleware');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');


const router = Router();

router.get("/", (req, res, next) => {

    try {
        const data = UserService.getAll();

        if (!data) {
            req.err = {
                code: 404,
                message: "Users not found"
            }
        }
        res.data = data;

    } catch (err) {
        req.err = {
            code: 500,
            message: err.message
        }
    } finally {
        next();
    }
}, responseMiddleware);


router.get("/:id", (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        const data = UserService.search({
            id
        });
        if (!data) {
            req.err = {
                code: 404,
                message: "user not found"
            }
        }
        res.data = data;
    } catch (err) {
        req.err = {
            code: 500,
            message: err.message
        }
    } finally {
        next();
    }
}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    const {
        id
    } = req.params;

    const data = UserService.delete(id);
    if (!data.length) {
        req.err = {
            code: 404,
            message: "user is not found"
        }
    }
    res.data = data;
    next();

}, responseMiddleware);

router.post("/",  (req, res, next) => {

    if (req.err) {
        next();
        return;
    }
    req.body.role = req.body.role || "user";
    res.data= UserService.create(req.body);

    next();

}, responseMiddleware);

router.put("/:id",  (req, res, next) => {
    const {
        id
    } = req.params;
    if (req.err) {
        next();
        return;
    }
    const data = UserService.update(id, req.body);
    if (!data) {
        req.err = {
            code: 404,
            message: "user not found"
        }
    }
    res.data = data;

    next();

}, responseMiddleware);


router.post("/login", (req, res, next) => {
    try {
        const {email, password} = req.body;
        const data = UserService.search({
            email, password
        });
        if (!data) {
            req.err = {
                code: 404,
                message: "user not found"
            }
        }
        res.data = data;
    } catch (err) {
        req.err = {
            code: 500,
            message: err.message
        }
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;