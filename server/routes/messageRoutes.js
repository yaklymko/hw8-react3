const {
    Router
} = require('express');

const MessageService = require('../services/messageService');

const {
    responseMiddleware
} = require('../middlewares/response.middleware');

const {
    createMessageValid,
    updateMessageValid
} = require('../middlewares/message.validation.middleware');

const router = Router();

router.get("/", (req, res, next) => {
    const data = MessageService.getAll();

    if (!data) {
        req.err = {
            code: 404,
            message: "messages not found"
        }
    }
    res.data = data;
    next();
}, responseMiddleware);


router.get("/:id", (req, res, next) => {
    const {
        id
    } = req.params;

    const data = MessageService.search({
        id
    });

    if (!data) {
        req.err = {
            code: 404,
            message: "message not found"
        }
    }
    res.data = data;

    next();

}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    const {
        id
    } = req.params;

    const data = MessageService.delete(id);

    if (!data.length) {
        req.err = {
            code: 404,
            message: "message is not found"
        }
    }
    res.data = data;

    next();

}, responseMiddleware);

router.post("/", createMessageValid, (req, res, next) => {

    if (req.err) {
        next();
        return;
    }
    res.data = MessageService.create(req.body) || {};

    next();

}, responseMiddleware);

router.put("/:id", updateMessageValid, (req, res, next) => {
    if (req.err) {
        next();
        return;
    }
    const {
        id
    } = req.params;

    const data = MessageService.update(id, req.body);

    if (!data) {
        req.err = {
            code: 404,
            message: "message is not found"
        }
    }
    res.data = data;

    next();


}, responseMiddleware);


module.exports = router;