const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll(){
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    create(data){
        return UserRepository.create(data);
    }

    delete(id){
        return UserRepository.delete(id);
    }

    update(id, data){
        if(this.search({id})){
            return UserRepository.update(id, data);
        } else {
            return null;
        }
    }
}

module.exports = new UserService();