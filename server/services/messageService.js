const {MessageRepository} = require("../repositories/messageRepository");
const {UserRepository} = require("../repositories/userRepository");
const {getRandomId} = require("../utils/hash");
const {search, getAll} = require("../services/userService");

class MessageService {
	getAll() {
		const items = MessageRepository.getAll();
		if (!items) {
			return null;
		}
		return items;
	}

	search(search) {
		const item = MessageRepository.getOne(search);
		if (!item) {
			return null;
		}
		return item;
	}

	create(data) {

		const creator = search({id: data.userId.toString()});
		data.likes = [];
		data.user = creator.email;
		data.avatar = "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg";


		MessageRepository.create(data);
	}

	delete(id) {
		return MessageRepository.delete(id);
	}

	update(id, data) {
		if (this.search({id})) {
			return MessageRepository.update(id, data);
		} else {
			return null;
		}
	}

}

module.exports = new MessageService();