import React from "react";
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";

import Chat from "./components/Chat/Chat";
import EditMessage from "./components/EditMessage/EditMessagePage";
import Login from "./components/Login/Login";
import Header from "./components/partials/Header/Header";
import UsersList from "./components/Users/UsersList";
import UserPage from "./components/UserPage/UserPage";
import Footer from "./components/partials/Footer/Footer";

import {connect} from "react-redux";
import {logout} from "./store/actions/users";

function App(props) {

	return (
		<React.Fragment>

			<Router>
				<Header currUser={props.currUser} onLogout={props.logout}/>

				<Switch>


					<Route path="/chat">
						{authProxy(props.currUser, <Chat/>)}
					</Route>

					<Route path="/login" >
						<Login currUser={props.currUser}/>
					</Route>

					<Route path="/messageEdit/:id">
						{authProxy(props.currUser, <EditMessage/>)}
					</Route>

					<Route path="/users">
						{adminProxy(props.currUser, <UsersList />)}
					</Route>

					<Route path="/user/:id">
						{adminProxy(props.currUser, <UserPage />)}
					</Route>

					<Route path="/user">
						{adminProxy(props.currUser, <UserPage />)}
					</Route>

					<Route path="/">
						<Redirect to={"/chat"}/>;
					</Route>

				</Switch>

			</Router>
			<Footer/>
		</React.Fragment>
	);
}

function authProxy(user, component){
	if(!user){
		return <Redirect to={"/login"}/>;
	} else {
		return component
	}
}

function adminProxy(user, component){
	if(!user){
		return <Redirect to={"/login"}/>;
	} else if(user.role==="admin")
		{
		return component
	} else {
		return <Redirect to={"/chat"}/>;
	}
}


function mapStateToProps(state) {
	return {currUser: state.users.currUser};
}

const mapDispatchToProps = (dispatch) => {
	return {
		logout: () => dispatch(logout()),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);