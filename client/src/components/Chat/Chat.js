import React from "react";

import Header from "./components/Header/Header";
import MessagesList from "./components/MessagesList/MessagesList";
import CreateMessageForm from "./components/CreateMessageForm/CreateMessageForm";
import Loader from "./components/Loader/Loader";

import "./Chat.css";

import {connect} from "react-redux";
import {addMessage, deleteMessage, requestMessages, setCurrUser, updateMessage} from "../../store/actions/chat";

class Chat extends React.Component {

	scrollChat = () => {
		const chatBox = document.getElementsByClassName("messages-wrapper")[0];
		chatBox.scrollTop = chatBox.scrollHeight;
	};

	componentDidMount() {
		this.props.requestMessages();
	}

	addMessage = (text) => {
		const userId = this.props.userId;

		this.props.addMessage({text, userId});
	};


	deleteMessage = (messageId) => {
		this.props.deleteMessage(messageId);
	};

	render() {

		const messagesArr = this.props.messages;
		const participants = new Set(messagesArr.map(m => m.user));
		const lastMessageAt = messagesArr.length ? messagesArr[messagesArr.length - 1].createdAt : null;

		return (

			<main id={"chatGame"}>
				{this.props.isFetching ? <Loader/> : ""}

				<Header chatName={"My chat"}
						participantsAmount={participants.size}
						messagesAmount={messagesArr.length}
						lastMessageTime={lastMessageAt || (new Date()).toString()}
				/>

				<MessagesList messages={messagesArr} userId={this.props.userId} deleteMessage={this.deleteMessage}/>

				<CreateMessageForm createMessage={this.addMessage} scrollChat={this.scrollChat}/>
			</main>
		);
	}
}


function mapStateToProps(state) {
	return {
		...state.chat,
		userId : state.users.currUser.id
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		requestMessages: () => dispatch(requestMessages()),
		addMessage: (m) => dispatch(addMessage(m)),
		deleteMessage: (id) => dispatch(deleteMessage(id)),
		setCurrUser: (id) => dispatch(setCurrUser(id)),
	};
};


export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Chat);


