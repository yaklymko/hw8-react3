import React from "react";
import {Link} from "react-router-dom";

import "./Header.css";


export default function Header(props) {
	return (
		<header id={"header"}>
			<h2>My chatek</h2>
			<Link to={"/login"} onClick={props.onLogout}>
				{props.currUser
					? `You are logged in as ${props.currUser.email}`
					: "Login"
				}
			</Link>
			<Link to={"/chat"}> Chat </Link>
			<Link to={"/users"}> Users List </Link>
		</header>
	);
}