import React from "react";
import  { Redirect } from 'react-router-dom'

import { useState } from "react";

import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

import {connect} from "react-redux";
import {login} from "../../store/actions/users";

function Login(props) {
	const initialState ={
		email : "",
		password: ""
	}

	const [values, setValues] = useState(initialState);


	if(props.currUser && props.currUser.id){
		if(props.currUser.role==="admin"){
			return <Redirect to={"users"}/>;
		} else {
			return <Redirect to={"chat"}/>;
		}
	}

	function handleChange (event){
		const { name, value } = event.target;
		event.persist();
		setValues({ ...values, [name]: value });
	}

	function handleLogin() {
		props.login(values.email, values.password);
		setValues(initialState);

	}

	return (
		<Modal
			show={true}
			backdrop="static"
			keyboard={false}
		>
			<Modal.Header>
				<Modal.Title>Login</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form.Group controlId="formBasicEmail">
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" onChange={handleChange} value={values.email || ""} name={"email"}/>
				</Form.Group>

				<Form.Group controlId="formBasicPass">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" onChange={handleChange} value={values.password || ""} name={"password"}/>
				</Form.Group>

			</Modal.Body>
			<Modal.Footer>
				<Button variant="primary" onClick={handleLogin}>Login</Button>
			</Modal.Footer>
		</Modal>);
}


function mapStateToProps(state) {
	return {};
}

const mapDispatchToProps = (dispatch) => {
	return {
		login : (email, pass) => dispatch(login(email, pass)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Login);