import React from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Link, useHistory, useLocation} from "react-router-dom";

import {connect} from "react-redux";
import {saveChanges, setEditTarget, setMessageText} from "../../store/actions/editModal";

import "./EditMessagePage.css";

function EditModal(props) {
	const location = useLocation();
	const history = useHistory();

	const queryStrArr = location.pathname.split("/");
	const currMessageId = queryStrArr[queryStrArr.length - 1];


	if (!props.messageText) {
		props.setEditTarget(currMessageId);
	}


	function onInput(e) {
		props.setText(e.target.value);
	}

	function handleChange() {
		props.saveChanges(currMessageId, props.messageText);
		exitEditMode();
	}

	function exitEditMode() {
		props.setText("");
		history.push("/chat");
	}

	return (

		<Modal
			show={true}
			backdrop="static"
			keyboard={false}
		>
			<Modal.Header>
				<Modal.Title>Edit message</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<input type="text" onChange={onInput} value={props.messageText || ""}/>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={exitEditMode}>
					<Link to={"/chat"}>Close</Link>
				</Button>
				<Button variant="primary" onClick={handleChange}>Edit Message</Button>
			</Modal.Footer>
		</Modal>);
}


function mapStateToProps(state) {
	return {
		...state.editModal,
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		setText: (t) => dispatch(setMessageText(t)),
		saveChanges: (id, t) => dispatch(saveChanges(id, t)),
		setEditTarget: (id) => dispatch(setEditTarget(id)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditModal);