import * as actionsTypes from "../actionsTypes/users";
import {FETCH_USERS_SUCCESS, FETCH_USER_SUCCESS} from "../actionsTypes/users";
import UserService from "../../services/user.service";


const initialState = {
	currUser: UserService.getCurrUser(),
	users: [],
	userData: {
		email: '',
		password: ''
	}
};


export default function userReducer(
	state = initialState,
	action
) {

	switch (action.type) {

	case actionsTypes.LOGIN_USER_SUCCESS: {
		const user = action.user;

		UserService.setCurrUser(user.id, user.email, user.role);
		return {
			...state,
			currUser: user,
		};
	}

	case actionsTypes.LOGOUT_USER: {
		UserService.logout();

		return {
			...state,
			currUser: null,
		};
	}

	case FETCH_USERS_SUCCESS: {
		return {
			...state,
			users: action.payload.users
		};
	}

	case FETCH_USER_SUCCESS: {
		const { userData } = action.payload;
		return {
			...state,
			userData
		};
	}


	default:
		return state;
	}
}

