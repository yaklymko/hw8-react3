import {combineReducers } from "redux";
import messagesReducer from "./chat";
import editModalReducer from "./editModal";
import usersReducer from "./users";

export default  combineReducers({
	chat: messagesReducer,
	editModal : editModalReducer,
	users : usersReducer,
});


