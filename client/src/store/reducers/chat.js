import * as actionsTypes from "../actionsTypes/chat";

const initialState = {

	isFetching: false,
	chatName: "MyChatek",
	messages: [],
	userId : 1
};


export default function chatReducer(
	state = initialState,
	action
) {
	switch (action.type) {

	case actionsTypes.FETCH_MESSAGES_SUCCESS:
		return {
			...state,
			isFetching: false,
			messages: action.messages,
		};

	case actionsTypes.FETCH_MESSAGES:
		return {
			...state,
			isFetching: true,
		};


	case actionsTypes.UPDATE_MESSAGE: {

		const targetMessage = state.messages.find(mess => mess.id === action.messageId);
		targetMessage.text = action.text;
		targetMessage.editedAt = (new Date()).toISOString();

		return {
			...state,
			messages : [...state.messages]
		};
	}

	case actionsTypes.SET_CURR_USER: {
		return {
			...state,
			userId : action.userId
		};
	}


	default:
		return state;
	}
}
