import * as actionsTypes from "../actionsTypes/editModal";

const initialState = {
	messageText: "",
};


export default function chatReducer(
	state = initialState,
	action
) {
	switch (action.type) {

	case actionsTypes.SET_TEXT:
		return {
			...state,
			messageText: action.text,
		};
	
	case actionsTypes.SET_TARGET_SUCCESS:
		return {
			...state,
			messageText: action.targetText,
		};


	default:
		return state;
	}
}

