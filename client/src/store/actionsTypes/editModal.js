export const SET_TEXT = "SET_TEXT_EDIT_MODE";

export const SET_TARGET = "SET_TARGET";

export const SET_TARGET_SUCCESS = "SET_TARGET_SUCCESS";

export const SAVE_CHANGES = "SAVE_CHANGES";