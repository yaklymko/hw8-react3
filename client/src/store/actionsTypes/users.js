export const LOGIN_USER = "LOGIN_USER";

export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";

export const LOGOUT_USER = "LOGOUT_USER";

export const ADD_USER = "ADD_USER";

export const UPDATE_USER = "UPDATE_USER";

export const DELETE_USER = "DELETE_USER";

export const FETCH_USERS = "FETCH_USERS";

export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";

export const FETCH_USER = "FETCH_USER";

export const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";