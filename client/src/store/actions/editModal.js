import * as actionsTypes from "../actionsTypes/editModal";

export function setMessageText(text){
	return{
		type : actionsTypes.SET_TEXT,
		text
	};
}

export function setEditTarget(targetId){
	return{
		type : actionsTypes.SET_TARGET,
		messageId : targetId,
	};
}

export function saveChanges(id, text){
	return{
		type : actionsTypes.SAVE_CHANGES,
		messageId : id,
		text : text
	};
}

