import * as actionsTypes from "../actionsTypes/chat";


export function requestMessages(){
	return{
		type : actionsTypes.FETCH_MESSAGES,
	};
}

export function addMessage({text, userId}){
	return{
		type : actionsTypes.ADD_MESSAGE,
		data : {text, userId},
	};
}

export function deleteMessage(messageId){
	return{
		type : actionsTypes.DELETE_MESSAGE,
		messageId : messageId,
	};
}

export function updateMessage(messageId, text){
	return{
		type : actionsTypes.UPDATE_MESSAGE,
		messageId,
		text
	};
}

export function setCurrUser(userId){
	return {
		type : actionsTypes.SET_CURR_USER,
		userId
	};
}

