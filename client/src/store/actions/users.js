import * as actionsTypes from "../actionsTypes/users";

export function login(email, password){
	return{
		type : actionsTypes.LOGIN_USER,
		body : {
			email, password
		}
	};
}

export function logout(){
	return{
		type : actionsTypes.LOGOUT_USER,
	};
}


export const addUser = data => ({
	type: actionsTypes.ADD_USER,
	payload: {
		data
	}
});

export const updateUser = (id, data) => ({
	type: actionsTypes.UPDATE_USER,
	payload: {
		id,
		data
	}
});

export const deleteUser = id => ({
	type: actionsTypes.DELETE_USER,
	payload: {
		id
	}
});

export const fetchUsers = () => ({
	type: actionsTypes.FETCH_USERS
});

export const fetchUser = id => ({
	type: actionsTypes.FETCH_USER,
	payload: {
		id
	}
});