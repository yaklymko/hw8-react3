import { all } from 'redux-saga/effects';
import messageSaga from "./messages";
import userPageSagas from "./users";

export default function* rootSaga() {
	yield all([
		messageSaga(),
		userPageSagas(),
	])
};