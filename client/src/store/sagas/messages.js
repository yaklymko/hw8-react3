import {apiUrl} from "../../config";
import {all, call, put, takeEvery} from "redux-saga/effects";
import {ADD_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS} from "../actionsTypes/chat";
import {SAVE_CHANGES, SET_TARGET, SET_TARGET_SUCCESS} from "../actionsTypes/editModal";

export function* fetchMessages(action) {
	try {
		// /messages/${action.payload.id}
		const resp = yield call(fetch, `${apiUrl}/messages`, {method: "GET"});
		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: FETCH_MESSAGES_SUCCESS, messages: parsedRes});
		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* createMessage(action) {
	try {
		// /messages/${action.payload.id}
		const resp = yield call(fetch, `${apiUrl}/messages`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(action.data)
		});

		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: FETCH_MESSAGES});
		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchCreateMessages() {
	yield takeEvery(ADD_MESSAGE, createMessage);
}


export function* deleteMessage(action) {
	try {
		// /messages/${action.payload.id}
		const resp = yield call(fetch, `${apiUrl}/messages/${action.messageId}`, {
			method: "DELETE",
		});

		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: FETCH_MESSAGES});
		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* getMessageText(action) {
	try {
		const resp = yield call(fetch, `${apiUrl}/messages/${action.messageId}`, {method: "GET"});

		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: SET_TARGET_SUCCESS, targetText: parsedRes.text});

		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchSetEditTarget() {
	yield takeEvery(SET_TARGET, getMessageText);
}

export function* updateMessage(action) {
	try {
		const resp = yield call(fetch, `${apiUrl}/messages/${action.messageId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({text: action.text})
		});

		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: FETCH_MESSAGES});
		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(SAVE_CHANGES, updateMessage);
}

export default function* userPageSagas() {
	yield all([
		watchFetchMessages(),
		watchCreateMessages(),
		watchDeleteMessage(),
		watchUpdateMessage(),
		watchSetEditTarget()
	]);
};