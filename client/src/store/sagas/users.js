import {apiUrl} from "../../config";
import {all, call, put, takeEvery} from "redux-saga/effects";
import {
	ADD_USER,
	DELETE_USER,
	FETCH_USER,
	FETCH_USER_SUCCESS,
	FETCH_USERS,
	LOGIN_USER,
	LOGIN_USER_SUCCESS,
	UPDATE_USER
} from "../actionsTypes/users";


export function* loginUser(action) {
	try {
		const resp = yield call(fetch, `${apiUrl}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(action.body)
		});

		const parsedRes = yield resp.json();
		if (resp.status === 200) {
			yield put({type: LOGIN_USER_SUCCESS, user: parsedRes});

		}

	} catch (error) {
		console.error("fetchMessages error:", error.message);
	}
}

function* watchLogin() {
	yield takeEvery(LOGIN_USER, loginUser);
}


export function* fetchUsers() {
	try {
		const resp = yield call(fetch, `${apiUrl}/users`, {method: "GET"});

		const parsedRes = yield resp.json();

		if (resp.status === 200) {
			yield put({type: "FETCH_USERS_SUCCESS", payload: {users: parsedRes}});
		}

	} catch (error) {
		console.error("fetchUsers error:", error.message);
	}
}

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* addUser(action) {

	const {email, password} = action.payload.data;
	try {
		const resp = yield call(fetch, `${apiUrl}/users`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({email, password})
		});

		if (resp.status === 200 || resp.status === 201) {
			yield put({type: FETCH_USERS});
		}

	} catch (error) {
		console.log("createUser error:", error.message);
	}
}

function* watchAddUser() {
	yield takeEvery(ADD_USER, addUser);
}

export function* updateUser(action) {

	const {email, password} = action.payload.data;

	try {
		const resp = yield call(fetch, `${apiUrl}/users/${action.payload.id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({email, password})
		});

		if (resp.status === 200 || resp.status === 201) {
			yield put({type: FETCH_USERS});
		}

	} catch (error) {
		console.log("updateUser error:", error.message);
	}
}

function* watchUpdateUser() {
	yield takeEvery(UPDATE_USER, updateUser);
}

export function* deleteUser(action) {
	try {
		const resp = yield call(fetch, `${apiUrl}/users/${action.payload.id}`, {method: "DELETE"});

		if (resp.status === 200 || resp.status === 201) {
			yield put({type: FETCH_USERS});
		}
	} catch (error) {
		console.log("deleteUser Error:", error.message);
	}
}

function* watchDeleteUser() {
	yield takeEvery(DELETE_USER, deleteUser);
}

export function* fetchUser(action) {
	try {
		const resp = yield call(fetch, `${apiUrl}/users/${action.payload.id}`, {method : "GET"});

		const parsedRes = yield resp.json();


		if (resp.status === 200 || resp.status === 201) {
			yield put({type: FETCH_USERS});
			yield put({type: FETCH_USER_SUCCESS, payload: {userData: parsedRes}});
		}
	} catch (error) {
		console.log("fetchUsers error:", error.message);
	}
}

function* watchFetchUser() {
	yield takeEvery(FETCH_USER, fetchUser);
}

export default function* userPageSagas() {
	yield all([
		watchLogin(),
		watchAddUser(),
		watchFetchUsers(),
		watchUpdateUser(),
		watchDeleteUser(),
		watchFetchUser(),
	]);
};