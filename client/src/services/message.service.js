import {defaultAvatar} from "../config";
import {getRandomId} from "../helpers/utils";

class MessageService{
	createMessage(text, username, userId) {
		const dateObj = new Date();
		return {
			avatar : defaultAvatar,
			createdAt : dateObj.toISOString(),
			editedAt : "",
			id : getRandomId(),
			text : text,
			user : username,
			userId : userId,
			likes:[],
		};
	}
}

export default new MessageService();