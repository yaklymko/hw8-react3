
class UserService{
	getCurrUser(){
		return JSON.parse(localStorage.getItem("currUser"));
	}

	setCurrUser(id, email,role){
		localStorage.setItem("currUser", JSON.stringify({id, email, role}));
	}

	logout(){
		localStorage.removeItem("currUser");
	}

}

export default new UserService();